export const portfolioData = () => {
  const portf = ref([
    {
      name: 'Gllacy',
      img: '002.jpg',
      imgMob: '',
      description: 'Дипломный проект HTML Academy. Статичная верстка',
      link: 'https://mikeiv.github.io/gllacy/',
    },
    {
      name: 'Nerds',
      img: '003.jpg',
      imgMob: '',
      description: 'Дипломный проект HTML Academy. Статичная верстка с адаптивом',
      link: 'https://mikeiv.github.io/nerds/',
    },
    {
      name: 'VoGoo',
      img: '001.jpg',
      imgMob: '001-mob.jpg',
      description: 'Коммерческий проект. Дизайн. Верстка',
      link: 'https://mikeiv.github.io/VoGoo/',
    },
    {
      name: 'Pink',
      img: '004.jpg',
      imgMob: '004-mob.jpg',
      description: 'Дипломный проект HTML Academy. Статичная верстка с адаптивом',
      link: 'https://mikeiv.github.io/pink/',
    },
    {
      name: 'Almatyathletics',
      img: '005.jpg',
      imgMob: '005-mob.jpg',
      description: 'Коммерческий проект. Дизайн. Верстка',
      link: 'https://mikeiv.github.io/almatyathletics/',
    },
    {
      name: 'Stickerlabel',
      img: '006.jpg',
      imgMob: '006-mob.jpg',
      description: 'Коммерческий проект. Дизайн. Верстка',
      link: 'https://mikeiv.github.io/stickerlabel/',
    },
    {
      name: 'Void',
      img: '007.jpg',
      imgMob: '007-mob.jpg',
      description: 'Коммерческий проект. Дизайн. Верстка',
      link: 'https://mikeiv.github.io/void/',
    },
    {
      name: 'Круизный дом',
      img: '008.jpg',
      imgMob: '008-mob.jpg',
      description: 'Коммерческий проект. Верстка. Создание логики взаимодействия компонентов на нативном js',
      link: 'https://www.mcruises.ru/',
    },
    {
      name: 'Elza Lashes',
      img: '009.jpg',
      imgMob: '009-mob.jpg',
      description: 'Коммерческий проект. Дизайн. Верстка',
      link: 'https://mikeiv.github.io/Elza Lashes/',
    },
    {
      name: 'Армянские новости',
      img: '010.jpg',
      imgMob: '010-mob.jpg',
      description: 'Коммерческий проект. Верстка по утвержденному дизайну',
      link: 'https://armenia.im/',
    },
    {
      name: 'SynergyHUB',
      img: '011.jpg',
      imgMob: '011-mob.jpg',
      description: 'Коммерческий проект. Командная работа. Разработка компонентов. Создание методов. Подключение API',
      link: 'https://synergyhub.ru/',
    },
    {
      name: 'SynergyGlobalTrade',
      img: '012.jpg',
      imgMob: '012-mob.jpg',
      description: 'Коммерческий проект. Командная работа. Разработка компонентов. Создание методов. Подключение API',
      link: 'https://sglobaltrade.ru/',
    },
  ]);

  return {
    portf,
  };
};
